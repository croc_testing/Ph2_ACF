FROM centos:centos7

ARG THREADS=4

RUN yum install -y --nogpgcheck git scl-utils cmake vim boost-devel epel-release centos-release-scl && \
    yum install -y --nogpgcheck pugixml-devel devtoolset-10 root && \
    curl https://ipbus.web.cern.ch/doc/user/html/_downloads/ipbus-sw.centos7.x86_64.repo -o /etc/yum.repos.d/ipbus-sw.repo && \
    yum groupinstall -y uhal controlhub

WORKDIR /software

COPY . /software/Ph2_ACF

RUN cd /software/Ph2_ACF && \
    git submodule init && git submodule update && \
    source ./setup.sh && \
    mkdir build && \
    cd build && \
    cmake .. && \
    make -j$THREADS

