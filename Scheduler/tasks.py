
def tool(tool, **kwargs):
    return f'{tool}{{{", ".join(f"{k}={v}" for k, v in kwargs.items())}}}'

thresholdScanParams = {
    200 : { 'vcalRange' : [0, 600], 'vcalStep' : 10 },
    600 : { 'vcalRange' : [0, 1000], 'vcalStep' : 15 }
}

def tuning(threshold):
    return [
        'StuckPixelScan',
        'NoiseScan',
        tool("GlobalThresholdTuning", targetThreshold=threshold),
        tool("ThresholdScanSparse", **thresholdScanParams[threshold]),
        'ResetEnableMask',
        tool("ThresholdEqualization", targetThreshold=threshold),
        tool("ThresholdScanSparse", **thresholdScanParams[threshold]),
        'StuckPixelScan',
        'NoiseScan',
        tool("GlobalThresholdTuning", targetThreshold=threshold),
        tool("ThresholdScan", **thresholdScanParams[threshold])
    ]

tasks = [
    # {
    #     'name': 'VrefTrimming',
    #     'tools': ['VrefTrimming'],
    #     'updateConfig': True
    # },
    # {
    #     'name': 'MuxScan',
    #     'tools': ['MuxScan'],
    #     'powerCycleAfter': True
    # },
    {
        'name': 'ThresholdScan_previous_TDAC',
        'tools': [
            'ResetEnableMask', 
            'StuckPixelScan', 
            'NoiseScan', 
            tool("ThresholdScan", **thresholdScanParams[200])
        ]
    },
    {
        'name': 'ThresholdScan_preirrad_TDACs',
        'config': 'initial_tuning',
        'tools': [
            'ResetEnableMask',
            'StuckPixelScan', 
            'NoiseScan',
            tool("ThresholdScanSparse", **thresholdScanParams[200]),
            tool("GlobalThresholdTuning", targetThreshold=200),
            tool("ThresholdScan", **thresholdScanParams[200])
        ]
    },
    {
        'name': 'TuningParameters',
        'tools': [
            'ResetEnableMask',
            'InjectionDelay',
            tool("GlobalThresholdTuning", targetThreshold=600), 
            'StuckPixelScan', 
            'NoiseScan',
            'ThresholdTuning',
            'AnalogScan', 'GainTuning', 'GainScan', 'AnalogScan',
            'ThresholdTuning',
            'ThresholdScanSparse',
            'AnalogScan'
        ],
        'updateConfig': True
    },
    {
        'name': 'Tuning',
        'tools': [
            'ResetEnableMask',
            *tuning(600)[:-4],
            *tuning(200)
        ],
        'updateConfig': True
    },
    {
        'name' : 'InTimeThreshold',
        'tools' : ['ThresholdScanInTime']
    },
    {
        'name': 'ThresholdOscillation',
        'tools': ['ThresholdOscillation']
    },
    {
        'name': 'ThresholdOscillationAsync',
        'tools': ['ThresholdOscillation'],
        "params": [
            {
                "tomlFile": "RD53B.toml",
                "keys": [["Registers", "HitSampleMode"]],
                "value": 0
            }
        ]
    },
    {
        'name': 'TimeWalk',
        'tools': ['TimeWalk']
    },
    {
        'name': 'TimeWalkAsync',
        'tools': ['TimeWalk'],
        "params": [
            {
                "tomlFile": "RD53B.toml",
                "keys": [["Registers", "HitSampleMode"]],
                "value": 0
            }
        ]
    }
]