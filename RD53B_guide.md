# `RD53BMiniDAQ`

This is the main executable for RD53B. It can be used with the following syntax:

`RD53BminiDAQ -f HW_DESCRIPTION [-t TOOLS_CONFIG] [-r] [-h] [-s] [TOOL]...`

- `HW_DESCRIPTION`: a Hardware Description (.xml) file
- `TOOLS_CONFIG`: a Tools Configuration (.toml) file
- `-r`: reset the Firmware
- `-h`: hide plot widnows (without this flag each plot created by a tool will be displayed)
- `-s`: save (or update) the Chip Configuration (.toml). It is important to use this flag when performing any kind of tuning otherwise the resulting chip configuration will be discarded when the program exits.
- `TOOL`: any tool defined in the `TOOLS_CONFIG`


# Configuration

There are 3 types of configuration files. The Hardware Description (`Settings/CROC.xml`) file contains all the information needed to establish the communication with the various devices such as the ip address of the board, the chip IDs, the port where each chip is connected, etc. A Chip Configuration file (`Settings/RD53B.toml`) contains the configuration of a single chip including global and pixel registers. It is recommended to use a different Chip Configuration file for each chip when multiple chips are used at the same time. The Tools Configuration file (`Settings/RD53BTools.toml`) defines the names, types and parameter values of all the tools that can be executed with `RD53BminiDAQ`.

## Hardware Description (.xml)

The hardware description file is an XML document with a `HwDescription` root node containing one or more `BeBoard` nodes.

Each `BeBoard` node contains a `connection` node with a `uri` property containing the IPBus connection string for that board and one or more `OpticalGroup` nodes.

Each `OpticalGroup` node contains one or more `Hybrid` nodes.

Each `Hybrid` node has an `id` property representing the FMC port to which the hybrid is connected contains one or more `CROC` nodes.

Each `CROC` node has the following properties:
- `id`: the chip ID
- `lane`: the lane within the DP cable that the chip should use for its output

`CROC` nodes contain a `Settings` node where chip registers can be configured (virtal register names are also accepted).

`Hybrid` nodes contain a `Global` node where chip registers can be configured for all chips of a hybrid.

## Chip Configuration (.toml)

The chip configuration TOML file contains the following sections

### Pixels

This section is used to configure the pixels and contains a field for each of the fields of the pixel configuration (if a field is missing the default value is used):
- enable (default: 1)
- enableInjections (default: 1)
- enableHitOr (default: 0)
- tdac (default: 16)

The value of each of these fields can be either:
- An integer in which case all pixels will be configured with this value
- The name of a CSV file as a string. If the file exists it should have 336 rows and 432 columns. If the file doesn't exist the default value is used for that field.

When the `-s` flag is used the software will create a CSV for each field that doesn't have a uniform configuration. If such a field was configured with a string, that string will be used as the filename. Otherwise, the default filename (`enable.csv`, `enableInjections.csv`, `enableHitOr.csv` or `tdac.csv`) will be used, unless such a file already exists in which case a number will be added as a suffix.

### CoreColumns

This section is used to configure the core columns and contains 2 fields (if a field is missing the default value is used)::
    - disable (default: [])
    - disableInjections (default: [])

The value of each of these fields is an array containing zero or more 2-element arrays each representing a range of core columns.

For example:
```
[CoreColumns]
disable = [[12, 14], [25, 50]]
```
would disable all core columns in the ranges 12 to 24 and 25 to 50.

### Registers

This section is used to configure the chip's registers. Each field in this section corresponds to a virtual register. Note that if you configure the same register via the chip configuration (.toml) and the hardware description (.xml) then the latter is prioratized. When the `-s` flag is used, the value of any register that was either read from or written to by the software will be saved here.

## Tools Configuration (.toml)

This file contains a section for each tool. Each of these sections must contain 2 fields:
- `type` is the name of the C++ class of the tool
- `args` is a TOML table (https://toml.io/en/v1.0.0#table) containing a field for each of the parameters of the tool (missing parameters get their corresponding default value)

# Tool Types

## RD53BInjectionTool

This tool is used to perform injections. Instead of sending the command stream required for the injections, the software simply configures the Firmware which performs the following cycle where each delay is configurable:

```mermaid
  graph LR;
      Prime-->|delayAfterPrime|Inject;
      Inject-->|delayAfterInject|Trigger;
      Trigger-->|delayAfterTrigger|Prime;
```

| Parameter | Default Value | Description |
|-----------|-------------|-------------|
| nInjections | 10 | Number of injections |
| triggerDuration | 10 | Number of triggers per injection |
| triggerLatency | 133 | The trigger latency to be used for all chips (in 40 MHz cycles) |
| injectionType | `"Analog"` | Either `"Digital"` or `"Analog"` |
| readoutPeriod | 0 | If `readoutPeriod` is higher than 0 then the injections will be broken up in groups of `readoutPeriod` injections |
| delayAfterPrime | 100 | The delay after the Prime step in the injection cycle in units of 100 ns|
| delayAfterPrime | 32 | The delay after the Inject step in the injection cycle in units of 100 ns|
| delayAfterTrigger | 800 | The delay after the Trigger step in the injection cycle in units of 100 ns|
| fineDelay | 0 | The fine delay to use for injections in units of 0.78125 ns |
| pulseDuration | 6 | The edge duration for the CAL commands (determines ToT for digital injections) |
| offset | `[0, 0]` | The coordinates of the top left pixel of the rectangular subset of the pixels that will be injected |
| size | `[0, 0]` | The size (number of rows and number of columns) of the rectangular subset of the pixels that will be injected |
| frameStep | 1 | Only 1 evrey frameStep frames of the injection pattern will be performed |
| disableUnusedPixels | `true` | Whether or not to disable pixels that don't belong to the current frame of the injection pattern. Pixels that are disabled from the chip configuration will not be enabled independently of the value of this parameter. |
| injectUnusedPixels | `false` | Whether or not to enable injections for pixels that don't belong to the current frame of the injection pattern. Pixels for which injections are disabled from the chip configuration will not be injected independently of the value of this parameter. |
| decoderThreads | `std::thread::hardware_concurrency()` | How many threads to use for event decoding |
| maskGen | 1 pixel per column per frame | Defines the injection pattern |


## RD53BThresholdScan

Performs analog injections using the specified injection tool for a range of values of `VCAL_HIGH` and estimates the threshold and noise of each pixel.

| Parameter | Default Value | Description |
|-----------|-------------|-------------|
| injectionTool | `RD53BInjectionTool{}` | A tool of type `RD53BInjectionTool` to be used for injections |
| vcalMed | 300 | The value of `VCAL_MED` |
| vcalRange | `[200, 800]` | The range of DeltaVCAL values to scan |
| vcalStep | 20 | The scan step in DeltaVCAL units |
| vcalHighDelayMs | 0 | The delay in milliseconds between setting `VCAL_HIGH` and injecting |
| fitScurves | `false` | Fit probit model using Newton-Raphson to estimate the threshold and noise |
| learningRage | 1.0 | The learning rate for the fitting of the probit model. Use lower values in case of divergence. |
| nIter | 50 | Number of iterations for the Newton-Raphson method |

## RD53BThresholdEqualization

Tunes TDAC such that the threshold of each pixel is as close as possible to the target threshold. Can optionally perform a threshold scan to estimate the current mean threshold and use it as the target threshold.

It uses a search method similar to binary search to find the best TDAC for each pixel without trying out all possible values.

| Parameter | Default Value | Description |
|-----------|-------------|-------------|
| thresholdScan | `RD53BThresholdScan{}` | A tool of type `RD53BThresholdScan` to be used for estimating the current mean threshold |
| injectionTool | `RD53BInjectionTool{}` | A tool of type `RD53BInjectionTool` to be used for injections during TDAC tuning |
| targetThreshold | 0 | The target threshold in DeltaVCAL units. If it's 0 a threshold scan will be performed and the mean threshold will be used as the target threshold. |
| initialTDAC | 16 | The initial TDAC to use for the search |
| nSteps | 6 | The number of TDAC values to test. If higher than 5 some TDAC values may be tested more than once. |

## RD53BGlobalThresholdTuning

Tunes GDAC (ie. `DAC_GDAC_L_LIN`, `DAC_GDAC_R_LIN` and `DAC_GDAC_M_LIN`) such that the overall occupancy is as close as possible to 50% for an injection charge equal to the target threshold.

| Parameter | Default Value | Description |
|-----------|-------------|-------------|
| targetThreshold | 300 | The target threshold in DeltaVCAL units |
| gdacRange | `[380, 600]` | The range of GDAC values to search in |
| vcalMed | 300 | The value of `VCAL_MED` |
| injectionTool | `RD53BInjectionTool{}` | A tool of type `RD53BInjectionTool` to be used for injections |
| maxStuckPixelRatio | 0.1 | The maximum fraction of the pixels that are allowed to be stuck |


## RD53BNoiseScan

Sends a number of triggers, measures occupancy and optionally disables pixels that are too noisy.

| Parameter | Default Value | Description |
|-----------|-------------|-------------|
| nTriggers | 1000000 |  The number of triggers to send |
| triggerPeriod | 100 | The delay between each trigger in units of 100 ns |
| triggerLatency | 133 | The trigger latency to be used for all chips (in 40 MHz cycles) |
| readoutPeriod | 0 | If it's higher than 0 then the triggers will be broken up in groups of `readoutPeriod` triggers |
| offset | `[0, 0]` | The coordinates of the top left pixel of the rectangular subset of the pixels that will be enabled |
| size | `[0, 0]` | The size (number of rows and number of columns) of the rectangular subset of the pixels that will be enabled |
| maskNoisyPixels | `false` | Whether or not to mask noisy pixels. |
| occupancyThreshold | `1e-6` | Pixels with higher occupancy than this are considered noisy |

## RD53BStuckPixelScan

Performs injections using a high injection charge possible and masks pixels with occupancy lower than the specified threshold.

| Parameter | Default Value | Description |
|-----------|-------------|-------------|
| vcal | 3795 | The charge to inject in DeltaVCAL units. | 
| vcalMed | 300 | The value of `VCAL_MED` |
| injectionTool | `RD53BInjectionTool{}` | A tool of type `RD53BInjectionTool` to be used for injections |
| occupancyThreshold | 0.9 | Pixels with occupancy lower than this are considered stuck |


## RD53BThresholdTuning

Tunes GDAC, LDAC and TDAC such that the threshold of each pixel is as close as possible to the target threshold.

This is done in 3 steps:
1. **GDAC** is tuned to the lowest value such that most pixels have a threshold **higher** than the target threshold with TDAC = **0**
2. **LDAC** is tuned to the lowest value such that most pixels have a threshold **lower** than the target threshold with TDAC = **31**
3. A tool of type `RD53BThresholdEqualization` is used to tune the TDAC

| Parameter | Default Value | Description |
|-----------|-------------|-------------|
| targetThreshold | 300 | The target threshold in DeltaVCAL units |
| gdacRange | `[380, 500]` | The range of GDAC values to search in |
| ldacRange | `[0, 200]` | The range of LDAC values to search in |
| vcalMed | 300 | The value of `VCAL_MED` |
| thresholdEqualization | `RD53BThresholdEqualization{}` | A tool of type `RD53BThresholdEqualization` to be used to tune the chip |
| belowThresholdPixelRatio | 0.01 | The fraction of pixels that are allowed to be below threshold |
| aboveThresholdPixelRatio | 0.01 | The fraction of pixels that are allowed to be above threshold |
| occupancyThresholdBelow | 0.9 | The occupancy above which pixels are assumed to have a threshold lower than the target threshold |
| occupancyThresholdAbove | 0.1 | The occupancy below which pixels are assumed to have a threshold higher than the target threshold | stuckPixelOccThreshold | 0.9 | The occupancy for injections of high charge below which pixels are considered stuck |

## RD53BRegReader

Reads all global registers and prints their value in the terminal. 