#ifndef RD53BDACCalib_H
#define RD53BDACCalib_H

#include <array>
#include <cmath>
#include <fstream>
#include <map>
#include <sstream>
#include <stdexcept>
#include <string>
#include <utility>
#include <vector>

#include "RD53B.h"
#include "RD53BCommands.h"
#include "RD53BInterface.h"
#include "RD53BRegister.h"
#include "RD53BATLASRegisters.h"
#include "RD53BCMSRegisters.h"
#include "RD53BTool.h"

#include "TAxis.h"
#include "TF1.h"
#include "TFile.h"
#include "TGraphErrors.h"

namespace RD53BTools {

template <class>
class RD53BDACCalib;

template <class F>
const auto ToolParameters<RD53BDACCalib<F>> = make_named_tuple(
    std::make_pair("dacCodes"_s, 3),
    std::make_pair("voltmeterId"_s, std::string("dacCalib")),
    std::make_pair("voltmeterChannelId"_s, std::string("dacCalib")),
    std::make_pair("voltmeterSamples"_s, 2),
    std::make_pair("dacRegisters"_s, std::vector<std::string>()),
    std::make_pair("muxInput"_s, std::vector<std::string>()),
    std::make_pair("dacMin"_s, std::vector<uint16_t>()),
    std::make_pair("dacMax"_s, std::vector<uint16_t>()),
    std::make_pair("currentMuxResistance"_s, -1.0f),
    std::make_pair("fitThresholdV"_s, INFINITY)
);

template <class F>
class RD53BDACCalib : public RD53BTool<RD53BDACCalib, F> {
  public:
    struct DACData { std::vector<float> code, volt, voltErr; };

    using Base = RD53BTool<RD53BDACCalib, F>;
    using Base::Base;
    using Base::param;
    using Results = ChipDataMap<std::map<RD53BConstants::Register, DACData>>;

    void init();

    Results run();

    void draw(const Results& results);

  private:
    void calibChip(Chip* chip, std::map<RD53BConstants::Register, DACData>& res);

    size_t nCod, nSamp;
    float imuxR;
    std::string vmetId, vmetChId;
    std::map<RD53BConstants::Register, uint16_t> muxSelecs;
    std::map<RD53BConstants::Register, bool> isCurr;
    std::vector<RD53BConstants::Register> regs;
    std::vector<uint16_t> min, max;
};

}

#endif
