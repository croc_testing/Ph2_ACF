#include "RD53BThresholdScan.h"

#include <TFile.h>
#include <TCanvas.h>
#include <TH1F.h>
#include <TH2F.h>
#include <TGaxis.h>

#include <math.h>
#include <experimental/array>

using namespace RD53BEventDecoding;

namespace RD53BTools {


template <class Flavor>
void RD53BThresholdScan<Flavor>::init() {
    vcalBins = xt::arange(param("vcalRange"_s)[0], param("vcalRange"_s)[1], param("vcalStep"_s));
}

template <class Flavor>
ChipDataMap<ThresholdEstimatorResults> RD53BThresholdScan<Flavor>::run(Task progress) {
    auto& chipInterface = Base::chipInterface();
    
    Base::for_each_chip([&] (auto* chip) {
        chipInterface.WriteReg(chip, Flavor::Reg::VCAL_MED, param("vcalMed"_s));
    });

    param("injectionTool"_s).param("enableToTReadout"_s) = false;
    
    // AsyncThresholdAnalyzer analyzer(*this, param("analyzerThreads"_s));
    AsyncWorkerPool<void> analyzer_pool(param("analyzerThreads"_s), param("maxAnalysisQueueSize"_s));
    ThresholdEstimator thresholdEstimator(
        vcalBins, 
        param("injectionTool"_s).param("nInjections"_s), 
        param("injectionTool"_s).param("triggerDuration"_s),
        param("epsilon"_s),
        {param("injectionTool"_s).size(0), param("injectionTool"_s).size(1)},
        {param("injectionTool"_s).offset(0), param("injectionTool"_s).offset(1)}
    );

    ChipDataMap<ThresholdEstimatorResults> results;
    Base::for_each_chip([&] (auto* chip) {
        results.insert({chip, ThresholdEstimatorResults(param("injectionTool"_s).size(0), param("injectionTool"_s).size(1), vcalBins.size())});
    });

    std::vector<std::future<void>> analysisFutures;

    param("injectionTool"_s).injectionScan(
        progress, 
        std::experimental::make_array(
            ScanRange(vcalBins.size(), [&] (size_t i) {
                Base::for_each_chip([&] (auto* chip) {
                    chipInterface.WriteReg(chip, Flavor::Reg::VCAL_HIGH, param("vcalMed"_s) + vcalBins(i));
                });
                if (param("vcalHighDelayMs"_s) > 0)
                    usleep(1000 * param("vcalHighDelayMs"_s));
            })
        ),
        [&] (auto frameId, auto&& events) {
            auto usedPixels = param("injectionTool"_s).generateInjectionMask(frameId);
            Base::for_each_chip([&] (auto* chip) {
                xt::xtensor<bool, 2> enabled = xt::view(usedPixels && chip->injectablePixels(), param("injectionTool"_s).rowRange(), param("injectionTool"_s).colRange());
                analysisFutures.push_back(analyzer_pool.enqueue_task([&, chip, enabled = std::move(enabled), events = std::move(events[chip])] () {
                    thresholdEstimator(events, enabled, results[chip]);
                }));
            });
        }
    );

    for (const auto& f : analysisFutures)
        f.wait();

    analyzer_pool.join();

    return results;
}


template <class... Ts>
void print_stats(const Ts&... args) {
    LOG(INFO) << std::setw(12) << " " << std::setw(12) << "mean" << std::setw(12) << "stddev" << std::setw(12) << "min" << std::setw(12) << "max";
    int unused[]{ (LOG(INFO) << std::setw(12) << std::get<0>(args) << 
                 std::setw(12) << xt::mean(std::get<1>(args))() << 
                 std::setw(12) << xt::stddev(std::get<1>(args))() << 
                 std::setw(12) << xt::amin(std::get<1>(args))() << 
                 std::setw(12) << xt::amax(std::get<1>(args))(), 0) ...};
    (void)unused;
}


template <class Flavor>
void RD53BThresholdScan<Flavor>::draw(const ChipDataMap<ThresholdEstimatorResults>& results) {
    Base::createRootFile();

    const size_t nInjections = param("injectionTool"_s).param("nInjections"_s);

    auto usedPixels = param("injectionTool"_s).usedPixels();

    Base::for_each_chip([&] (RD53B<Flavor>* chip) {
        auto* dir = Base::createRootFileDirectory(chip);

        const auto& chip_results = results.at(chip);

        auto enabled = xt::view(usedPixels && chip->pixelConfig().enable, rowRange(), colRange());

        size_t maxHitCount = std::max(nInjections, (size_t)std::round(xt::amax(chip_results.occupancy)() * nInjections));
        size_t maxHitCountRaw = std::max(nInjections, (size_t)std::round(xt::amax(chip_results.occupancyRaw)() * nInjections));

        const auto& threshold = chip_results.threshold; //thresholdNoiseAndPseudoR2[0][chip];
        const auto& noise = chip_results.noise; //thresholdNoiseAndPseudoR2[1][chip];
        const auto& pseudoR2 = chip_results.pseudoR2; // thresholdNoiseAndPseudoR2[2][chip];

        double meanR2 = xt::mean(pseudoR2)();
        double stddevR2 = xt::stddev(pseudoR2)();

        xt::xtensor<bool, 2> valid = pseudoR2 > std::max(meanR2 - 10 * stddevR2, 0.1);

        xt::xtensor<size_t, 2> scurves = xt::zeros<size_t>({vcalBins.size(), maxHitCount + 1});
        xt::xtensor<size_t, 2> scurvesRaw = xt::zeros<size_t>({vcalBins.size(), maxHitCountRaw + 1});
        xt::xtensor<size_t, 2> good_scurves = xt::zeros<size_t>({vcalBins.size(), maxHitCount + 1});
        xt::xtensor<size_t, 2> bad_scurves = xt::zeros<size_t>({vcalBins.size(), maxHitCount + 1});
        xt::xtensor<size_t, 2> scurves_probit = xt::zeros<size_t>({vcalBins.size(), maxHitCount + 1});
        xt::xtensor<size_t, 2> scurves_differential = xt::zeros<size_t>({vcalBins.size(), maxHitCount + 1});

        for (size_t row = 0; row < chip_results.occupancy.shape()[0]; ++row) {
            for (size_t col = 0; col < chip_results.occupancy.shape()[1]; ++col) {
                if (enabled(row, col)) {
                    for (size_t i = 0; i < vcalBins.size(); ++i) {
                        scurves(i, std::round(nInjections * chip_results.occupancy(row, col, i))) += 1;
                        scurvesRaw(i, std::round(nInjections * chip_results.occupancyRaw(row, col, i))) += 1;
                    }
                    if (valid(row, col)) {
                        for (size_t i = 0; i < vcalBins.size(); ++i)
                            good_scurves(i, std::round(nInjections * chip_results.occupancy(row, col, i))) += 1;
                    }
                    else {
                        for (size_t i = 0; i < vcalBins.size(); ++i)
                            bad_scurves(i, std::round(nInjections * chip_results.occupancy(row, col, i))) += 1;
                    }
                    if (valid(row, col)) {
                        for (size_t i = 0; i < vcalBins.size(); ++i)
                            scurves_probit(i, std::round(nInjections * normal_cdf((vcalBins(i) - threshold(row, col)) / noise(row, col)))) += 1;
                    }
                    for (size_t i = 0; i < vcalBins.size(); ++i)
                        scurves_differential(i, std::round(nInjections * normal_cdf((vcalBins(i) - chip_results.threshold_diff(row, col)) / chip_results.noise_diff(row, col)))) += 1;
                }
            }
        }

        dir->mkdir("S-Curves")->cd();
        Base::drawHist2D(scurvesRaw, "Raw S-Curves", vcalBins(0), vcalBins.periodic(-1), 0, maxHitCountRaw + 1, "Delta VCAL", "Occupancy", "# of Pixels");
        Base::drawHist2D(scurves, "Filtered S-Curves", vcalBins(0), vcalBins.periodic(-1), 0, maxHitCount + 1, "Delta VCAL", "Occupancy", "# of Pixels");
        Base::drawHist2D(good_scurves, "S-Curves for which the probit model fit succdeded", vcalBins(0), vcalBins.periodic(-1), 0, maxHitCount + 1, "Delta VCAL", "Occupancy", "# of Pixels");
        Base::drawHist2D(bad_scurves, "S-Curves for which the probit model fit failed", vcalBins(0), vcalBins.periodic(-1), 0, maxHitCount + 1, "Delta VCAL", "Occupancy", "# of Pixels");
        Base::drawHist2D(scurves_probit, "Fitted S-Curves (probit)", vcalBins(0), vcalBins.periodic(-1), 0, maxHitCount + 1, "Delta VCAL", "Occupancy", "# of Pixels");
        Base::drawHist2D(scurves_differential, "Fitted S-Curves (differential)", vcalBins(0), vcalBins.periodic(-1), 0, maxHitCount + 1, "Delta VCAL", "Occupancy", "# of Pixels");

        xt::xtensor<double, 1> filtered_threshold = xt::filter(threshold, valid);
        xt::xtensor<double, 1> filtered_noise = xt::filter(noise, valid);
        xt::xtensor<double, 1> filtered_pseudoR2 = xt::filter(pseudoR2, enabled);

        LOG(INFO) << "Enabled pixels: " << xt::count_nonzero(usedPixels && chip->injectablePixels())();
        LOG(INFO) << "The probit model gave an acceptable fit for " << xt::count_nonzero(valid)() << " pixels";
        LOG(INFO) << "Probit model estimation results:";
        print_stats(
            std::tie("Threshold", filtered_threshold),
            std::tie("Noise", filtered_noise),
            std::tie("Pseudo-R^2", filtered_pseudoR2)
        );

        dir->mkdir("Threshold & Noise (probit model)")->cd();
        Base::drawMap(xt::where(valid, threshold, xt::zeros_like(threshold)), "Threshold Map (probit model)", "Threshold", param("injectionTool"_s).offset(0), param("injectionTool"_s).offset(1));

        Base::drawHist(filtered_threshold, "Threshold Distribution (probit model)", 150, vcalBins(0), vcalBins.periodic(-1), "Delta VCAL");

        Base::drawMap(xt::where(valid, noise, xt::zeros_like(noise)), "Noise Map (probit model)", "Noise", param("injectionTool"_s).offset(0), param("injectionTool"_s).offset(1));

        double maxNoise = 1.1 * xt::amax(filtered_noise)();

        Base::drawHist(filtered_noise, "Noise Distribution (probit model)", 100, 0, maxNoise, "Delta VCAL");
        
        Base::drawMap(pseudoR2, "pseudo-R^2 Map", "pseudo-R^2", param("injectionTool"_s).offset(0), param("injectionTool"_s).offset(1));
        Base::drawHist(filtered_pseudoR2, "pseudo-R^2 Distribution", 101, 0, 1.01, "pseudo-R^2");


        xt::xtensor<double, 1> filtered_threshold_diff = xt::filter(chip_results.threshold_diff, valid);
        xt::xtensor<double, 1> filtered_noise_diff = xt::filter(chip_results.noise_diff, valid);

        LOG(INFO) << "Non-parametrc (differential) estimation results:" << std::setprecision(5);
        print_stats(
            std::tie("Threshold", filtered_threshold_diff),
            std::tie("Noise", filtered_noise_diff)
        );

        dir->mkdir("Threshold & Noise (differential)")->cd();        

        Base::drawMap(chip_results.threshold_diff, "Threshold Map (differential)", "Threshold", param("injectionTool"_s).offset(0), param("injectionTool"_s).offset(1));

        Base::drawHist(xt::filter(chip_results.threshold_diff, enabled), "Threshold Distribution (differential)", 150, vcalBins(0), vcalBins.periodic(-1), "Delta VCAL");

        Base::drawMap(chip_results.noise_diff, "Noise Map (differential)", "Noise", param("injectionTool"_s).offset(0), param("injectionTool"_s).offset(1));

        double maxNoiseDiff = 1.1 * xt::amax(chip_results.noise_diff)();

        Base::drawHist(xt::filter(chip_results.noise_diff, enabled), "Noise Distribution (differential)", 100, 0, maxNoiseDiff, "Delta VCAL");
    });
}

template class RD53BThresholdScan<RD53BFlavor::ATLAS>;
template class RD53BThresholdScan<RD53BFlavor::CMS>;

}