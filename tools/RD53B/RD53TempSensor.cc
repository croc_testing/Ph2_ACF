#include "RD53TempSensor.h"

#include "../ProductionTools/ITchipTestingInterface.h"

#include "../DQMUtils/RD53TempSensorHistograms.h"


namespace RD53BTools {

template <class Flavor>
ChipDataMap<typename RD53TempSensor<Flavor>::ChipResults> RD53TempSensor<Flavor>::run() {
	constexpr float T0C = 273.15;         // [Kelvin]
	constexpr float kb  = 1.38064852e-23; // [J/K]
	constexpr float e   = 1.6021766208e-19;
	constexpr float R   = 15; // By circuit design
	
	ChipDataMap<ChipResults> results;
	auto& chipInterface = Base::chipInterface();

	Ph2_ITchipTesting::ITpowerSupplyChannelInterface dKeithley2410(Base::system().fPowerSupplyClient, "TestKeithley", "Front");
	dKeithley2410.setupKeithley2410ChannelSense(VOLTAGESENSE, 2.0);	

	uint16_t sensorConfigData;

	Base::for_each_chip([&] (auto* chip) {
		
		auto& idealityFactor = results[chip].idealityFactor;
		auto& valueLow = results[chip].valueLow;
		auto& valueHigh = results[chip].valueHigh;
		auto& calibDV = results[chip].calibDV;
		auto& calibNTCtemp = results[chip].calibNTCtemp;
		auto& calibSenstemp = results[chip].calibSenstemp;
	
		for(int mode=0;mode<2;mode++){
			if(mode == 0){ //Low power mode
				chipInterface.WriteReg(chip, "DAC_KRUM_CURR_LIN", 0);
				chipInterface.WriteReg(chip, "DAC_LDAC_LIN", 0);
				chipInterface.WriteReg(chip, "DAC_COMP_LIN", 0);
				chipInterface.WriteReg(chip, "DAC_REF_KRUM_LIN", 0);
				chipInterface.WriteReg(chip, "EnCoreCol_0", 0);
				chipInterface.WriteReg(chip, "EnCoreCol_1", 0);
				chipInterface.WriteReg(chip, "EnCoreCol_2", 0);
				chipInterface.WriteReg(chip, "EnCoreCol_3", 0);
				usleep(30000000);
			} else if(mode == 1){ //Standard power mode
				chipInterface.WriteReg(chip, "DAC_KRUM_CURR_LIN", 70);
				chipInterface.WriteReg(chip, "DAC_LDAC_LIN", 110);
				chipInterface.WriteReg(chip, "DAC_COMP_LIN", 110);
				chipInterface.WriteReg(chip, "DAC_REF_KRUM_LIN", 360);
				chipInterface.WriteReg(chip, "EnCoreCol_0", 0b0010010010010010);
				chipInterface.WriteReg(chip, "EnCoreCol_1", 0b1001001001001001);
				chipInterface.WriteReg(chip, "EnCoreCol_2", 0b0100100100100100);
				chipInterface.WriteReg(chip, "EnCoreCol_3", 0b010010);
				usleep(30000000);
			}
			for(int sensor=0;sensor<8;sensor++){
				chipInterface.WriteReg(chip, "MonitorEnable", 1); //Choose NTC MUX entry
				chipInterface.WriteReg(chip, "VMonitor", 0b000010);
				chipInterface.WriteReg(chip, "MonitorConfig", sensor_VMUX[sensor]); //Choose MUX entry
				calibNTCtemp[sensor][mode] = chipInterface.ReadHybridTemperature(chip);
				
				if(sensor >= 2){
					valueHigh = 0;
					// Get high bias voltage
					for(int sensorDEM=0;sensorDEM<16;sensorDEM+=7){
						if(sensor < 6){
							sensorConfigData = bits::pack<1, 4, 1, 1, 4, 1>(true, sensorDEM, 0, true, sensorDEM, 0);
							chipInterface.WriteReg(chip, "MON_SENS_SLDO", sensorConfigData);
						}else{ //center sensors use different bias
							sensorConfigData = bits::pack<1, 4, 1>(true, sensorDEM, 0);
							chipInterface.WriteReg(chip, "MON_SENS_ACB", sensorConfigData);
						}
						valueHigh += dKeithley2410.getVoltage();
					}
					valueHigh = valueHigh/3;
					
					valueLow = 0;
					// Get low bias voltage
					for(int sensorDEM=0;sensorDEM<16;sensorDEM+=7){
						if(sensor < 6){
							sensorConfigData = bits::pack<1, 4, 1, 1, 4, 1>(true, sensorDEM, 1, true, sensorDEM, 1);
							chipInterface.WriteReg(chip, "MON_SENS_SLDO", sensorConfigData);
						}else{ //center sensors use different bias
							sensorConfigData = bits::pack<1, 4, 1>(true, sensorDEM, 1);
							chipInterface.WriteReg(chip, "MON_SENS_ACB", sensorConfigData);
						}
						valueLow += dKeithley2410.getVoltage();
					}
					valueLow = valueLow/3;
					calibDV[sensor][mode] = valueLow - valueHigh;
				}else{ //Poly sensors do not have a bias difference
					calibDV[sensor][mode] = dKeithley2410.getVoltage();
				}
				
				if (mode == 1){
					double NTCfactor = calibNTCtemp[sensor][0]-power[0]*(calibNTCtemp[sensor][1]-calibNTCtemp[sensor][0])/(power[1]-power[0]);
					idealityFactor[sensor] = e/(kb*log(R))*((calibDV[sensor][0]*power[1]-power[0]*calibDV[sensor][1])/((power[1]-power[0])*(NTCfactor+T0C)));
					calibSenstemp[sensor][0] = (e/(kb*log(R)))*(calibDV[sensor][0])/idealityFactor[sensor]-T0C;
					calibSenstemp[sensor][1] = (e/(kb*log(R)))*(calibDV[sensor][1])/idealityFactor[sensor]-T0C;
				}
			}
		}
	});
	return results;
}

    
template <class Flavor>
void RD53TempSensor<Flavor>::draw(const ChipDataMap<ChipResults>& results) const {
	for (const auto& item : results) {
		TempSensorHistograms* histos = new TempSensorHistograms;
		histos->fillTC(
			item.second.idealityFactor, 
			item.second.calibNTCtemp, 
			item.second.calibSenstemp, 
			power
		);
	}
}

template <class Flavor>
const double RD53TempSensor<Flavor>::power[];

template <class Flavor>
const int RD53TempSensor<Flavor>::sensor_VMUX[];

template class RD53TempSensor<RD53BFlavor::ATLAS>;
template class RD53TempSensor<RD53BFlavor::CMS>;

}

