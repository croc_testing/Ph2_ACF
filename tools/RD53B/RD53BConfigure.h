#ifndef RD53BConfigure_H
#define RD53BConfigure_H

#include "RD53BTool.h"

namespace RD53BTools {

template <class>
struct RD53BConfigure; // forward declaration

template <class Flavor>
const auto ToolParameters<RD53BConfigure<Flavor>> = make_named_tuple(
    std::make_pair("registers"_s, std::map<std::string, size_t>()),
    std::make_pair("resetTdac"_s, false),
    std::make_pair("tdac"_s, 16u),
    std::make_pair("resetEnable"_s, false),
    std::make_pair("enable"_s, true),
    std::make_pair("resetEnableInjections"_s, false),
    std::make_pair("enableInjections"_s, 16u),
    std::make_pair("resetEnableHitOr"_s, false),
    std::make_pair("enableHitOr"_s, 16u)
);

template <class Flavor>
struct RD53BConfigure : public RD53BTool<RD53BConfigure, Flavor> {
    using Base = RD53BTool<RD53BConfigure, Flavor>;
    using Base::Base;
    using Base::param;

    bool run(Task progress) const {
        Base::for_each_chip([&] (auto chip) {
            for (auto& item : param("registers"_s)) {
                Base::chipInterface().ConfigureReg(chip, item.first, item.second);
            }
            if (param("resetTdac"_s))
                chip->pixelConfig().tdac.fill(param("tdac"_s));
            if (param("resetEnable"_s))
                chip->pixelConfig().enable.fill(param("enable"_s));
            if (param("resetEnableInjections"_s))
                chip->pixelConfig().enableInjections.fill(param("enableInjections"_s));
            if (param("resetEnableHitOr"_s))
                chip->pixelConfig().enableHitOr.fill(param("enableHitOr"_s));
            Base::chipInterface().UpdatePixelConfig(
                chip, 
                param("resetTdac"_s), 
                param("resetEnable"_s) || param("resetEnableInjections"_s) || param("resetEnableHitOr"_s)
            );
        });
        return true;
    }
};

}

#endif