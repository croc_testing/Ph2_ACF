#include "RD53TsensCalibration.h"

#include "../ProductionTools/ITchipTestingInterface.h"

#include "../DQMUtils/RD53TsensCalibrationHistograms.h"


namespace RD53BTools {

template <class Flavor>
ChipDataMap<typename RD53TsensCalibration<Flavor>::ChipResults> RD53TsensCalibration<Flavor>::run() {
	constexpr float T0C = 273.15;         // [Kelvin]
	constexpr float kb  = 1.38064852e-23; // [J/K]
	constexpr float e   = 1.6021766208e-19;
	constexpr float R   = 15; // By circuit design
	
	ChipDataMap<ChipResults> results;
	auto& chipInterface = Base::chipInterface();
	uint16_t sensorConfigData;
	
	std::string line;
	std::ifstream ADCFile("Results/ADCCalibration.txt");
	if (ADCFile.is_open())
	{
		std::getline(ADCFile, line);
		ADCintercept = std::stod(line);
		std::getline(ADCFile, line);
		ADCslope = std::stod(line);
		ADCFile.close();
	}
	else{
		LOG(INFO) << BOLDMAGENTA << "Unable to find ADC calibration, will use default\n" << RESET;
		LOG(INFO) << BOLDMAGENTA << "ADCintercept = " << BOLDYELLOW << ADCintercept << BOLDMAGENTA << "; ADCslope = " << BOLDYELLOW << ADCslope << RESET;
	}

	Base::for_each_chip([&] (auto* chip) {
		auto& idealityFactor = results[chip].idealityFactor;
		auto& valueLow = results[chip].valueLow;
		auto& valueHigh = results[chip].valueHigh;
		auto& calibDV = results[chip].calibDV;
		auto& calibNTCtemp = results[chip].calibNTCtemp;
		auto& calibSenstemp = results[chip].calibSenstemp;
	
		for(int mode=0;mode<2;mode++){
			if(mode == 0){ //Low power mode
				chipInterface.WriteReg(chip, "DAC_KRUM_CURR_LIN", 0);
				chipInterface.WriteReg(chip, "DAC_LDAC_LIN", 0);
				chipInterface.WriteReg(chip, "DAC_COMP_LIN", 0);
				chipInterface.WriteReg(chip, "DAC_REF_KRUM_LIN", 0);
				chipInterface.WriteReg(chip, "EnCoreCol_0", 0);
				chipInterface.WriteReg(chip, "EnCoreCol_1", 0);
				chipInterface.WriteReg(chip, "EnCoreCol_2", 0);
				chipInterface.WriteReg(chip, "EnCoreCol_3", 0);
				usleep(30000000);
			} else if(mode == 1){ //Standard power mode
				chipInterface.WriteReg(chip, "DAC_KRUM_CURR_LIN", 70);
				chipInterface.WriteReg(chip, "DAC_LDAC_LIN", 110);
				chipInterface.WriteReg(chip, "DAC_COMP_LIN", 110);
				chipInterface.WriteReg(chip, "DAC_REF_KRUM_LIN", 360);
				chipInterface.WriteReg(chip, "EnCoreCol_0", 0b0010010010010010);
				chipInterface.WriteReg(chip, "EnCoreCol_1", 0b1001001001001001);
				chipInterface.WriteReg(chip, "EnCoreCol_2", 0b0100100100100100);
				chipInterface.WriteReg(chip, "EnCoreCol_3", 0b010010);
				usleep(30000000);
			}
			for(int sensor=0;sensor<8;sensor++){
				chipInterface.WriteReg(chip, "MonitorEnable", 1); //Choose NTC MUX entry
				chipInterface.WriteReg(chip, "VMonitor", 0b000010);
				chipInterface.WriteReg(chip, "MonitorConfig", sensor_VMUX[sensor]); //Choose MUX entry
				calibNTCtemp[sensor][mode] = chipInterface.ReadHybridTemperature(chip);
				
				if(sensor >= 2){
					valueHigh = 0;
					// Get high bias voltage
					for(int sensorDEM=0;sensorDEM<16;sensorDEM+=7){
						if(sensor < 6){
							sensorConfigData = bits::pack<1, 4, 1, 1, 4, 1>(true, sensorDEM, 0, true, sensorDEM, 0);
							chipInterface.WriteReg(chip, "MON_SENS_SLDO", sensorConfigData);
						}else{ //center sensors use different bias
							sensorConfigData = bits::pack<1, 4, 1>(true, sensorDEM, 0);
							chipInterface.WriteReg(chip, "MON_SENS_ACB", sensorConfigData);
						}
						usleep(1000000);
						chipInterface.SendGlobalPulse(chip, {"ADCStartOfConversion"}); //ADC start conversion
						valueHigh += chipInterface.ReadReg(chip, "MonitoringDataADC") * ADCslope + ADCintercept;
					}
					valueHigh = valueHigh/3;
					usleep(5000000);
					
					valueLow = 0;
					// Get low bias voltage
					for(int sensorDEM=0;sensorDEM<16;sensorDEM+=7){
						if(sensor < 6){
							sensorConfigData = bits::pack<1, 4, 1, 1, 4, 1>(true, sensorDEM, 1, true, sensorDEM, 1);
							chipInterface.WriteReg(chip, "MON_SENS_SLDO", sensorConfigData);
						}else{ //center sensors use different bias
							sensorConfigData = bits::pack<1, 4, 1>(true, sensorDEM, 1);
							chipInterface.WriteReg(chip, "MON_SENS_ACB", sensorConfigData);
						}
						usleep(1000000);
						chipInterface.SendGlobalPulse(chip, {"ADCStartOfConversion"}); //ADC start conversion
						valueLow += chipInterface.ReadReg(chip, "MonitoringDataADC") * ADCslope + ADCintercept;
					}
					valueLow = valueLow/3;
					calibDV[sensor][mode] = valueLow - valueHigh;
				}else{ //Poly sensors do not have a bias difference
					usleep(1000000);
					chipInterface.SendGlobalPulse(chip, {"ADCStartOfConversion"}); //ADC start conversion
					calibDV[sensor][mode] = chipInterface.ReadReg(chip, "MonitoringDataADC") * ADCslope + ADCintercept;
				}
				
				if (mode == 1){
					double NTCfactor = calibNTCtemp[sensor][0]-power[0]*(calibNTCtemp[sensor][1]-calibNTCtemp[sensor][0])/(power[1]-power[0]);
					idealityFactor[sensor] = e/(kb*log(R))*((calibDV[sensor][0]*power[1]-power[0]*calibDV[sensor][1])/((power[1]-power[0])*(NTCfactor+T0C)));
					calibSenstemp[sensor][0] = (e/(kb*log(R)))*(calibDV[sensor][0])/idealityFactor[sensor]-T0C;
					calibSenstemp[sensor][1] = (e/(kb*log(R)))*(calibDV[sensor][1])/idealityFactor[sensor]-T0C;
				}
			}
		}
	});
	return results;
}

    
template <class Flavor>
void RD53TsensCalibration<Flavor>::draw(const ChipDataMap<ChipResults>& results) const {
	for (const auto& item : results) {
		TsensCalibrationHistograms* histos = new TsensCalibrationHistograms;
		histos->fillTC(
			item.second.idealityFactor, 
			item.second.calibNTCtemp, 
			item.second.calibSenstemp, 
			power
		);
	}
}

template <class Flavor>
const double RD53TsensCalibration<Flavor>::power[];

template <class Flavor>
const int RD53TsensCalibration<Flavor>::sensor_VMUX[];

template class RD53TsensCalibration<RD53BFlavor::ATLAS>;
template class RD53TsensCalibration<RD53BFlavor::CMS>;

}

