#ifndef RD53BNoiseFloor_H
#define RD53BNoiseFloor_H

#include "RD53BTool.h"
#include "RD53BNoiseScan.h"

namespace RD53BTools {

template <class>
struct RD53BNoiseFloor; // forward declaration

template <class Flavor>
const auto ToolParameters<RD53BNoiseFloor<Flavor>> = make_named_tuple(
    std::make_pair("noiseScan"_s, RD53BNoiseScan<Flavor>()),
    std::make_pair("GDACRange"_s, std::vector<size_t>({360, 500})),
    std::make_pair("GDACStep"_s, 1u)
);



template <class Flavor>
struct RD53BNoiseFloor : public RD53BTool<RD53BNoiseFloor, Flavor> {
    using Base = RD53BTool<RD53BNoiseFloor, Flavor>;
    using Base::Base;
    using Base::param;

    struct ChipResults {
        xt::xtensor<size_t, 3> hitCount;
        xt::xtensor<double, 3> meanToT;
    };

    void init();

    ChipDataMap<ChipResults> run(Task progress);

    void draw(const ChipDataMap<ChipResults>& result);

private:
    xt::xtensor<double, 1> GDACBins;
};

}

#endif