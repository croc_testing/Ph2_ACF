#ifndef RD53BTimeWalk_H
#define RD53BTimeWalk_H

#include "RD53BInjectionTool.h"

#include "../Utils/ThresholdEstimator.h"

class TTree;

namespace RD53BTools {

template <class>
struct RD53BTimeWalk; // forward declaration

template <class Flavor>
const auto ToolParameters<RD53BTimeWalk<Flavor>> = make_named_tuple(
    std::make_pair("injectionTool"_s, RD53BInjectionTool<Flavor>()),
    std::make_pair("vcalMed"_s, 300u),
    std::make_pair("vcalRange"_s, std::vector<size_t>({200, 800})),
    std::make_pair("nVcalSteps"_s, 30u),
    std::make_pair("logScaleVcal"_s, true),
    std::make_pair("maxDelay"_s, 32u),
    std::make_pair("maxFineDelay"_s, 32u),
    std::make_pair("fineDelayStep"_s, 1u),
    std::make_pair("storeHits"_s, false),
    std::make_pair("analyzerThreads"_s, 1u),
    std::make_pair("maxNPlots"_s, 100ul),
    std::make_pair("pickRandomPixels"_s, true)
);

template <class Flavor>
struct RD53BTimeWalk : public RD53BTool<RD53BTimeWalk, Flavor> {
    using Base = RD53BTool<RD53BTimeWalk, Flavor>;
    using Base::Base;
    using Base::param;

    struct ChipResults {
        // xt::xarray<RD53BEventDecoding::RD53BEvent> events;
        xt::xtensor<size_t, 2> tornado;
        xt::xtensor<double, 3> timewalk;
        // xt::xtensor<double, 3> sigma;
    };

    using Result = ChipDataMap<ChipResults>;

    void init();
    Result run(Task progress);
    void draw(const Result& lateHitRatio);

private:
    ChipDataMap<TTree*> hitTrees;
    size_t nVcalSteps;
    size_t nDelaySteps;
    xt::xtensor<double, 1> vcalBins;
};

}

#endif