#include "RD53TsensReadoutADC.h"

#include "../ProductionTools/ITchipTestingInterface.h"

#include "../DQMUtils/RD53TsensReadoutADCHistograms.h"


namespace RD53BTools {


template <class Flavor>
ChipDataMap<typename RD53TsensReadoutADC<Flavor>::ChipResults> RD53TsensReadoutADC<Flavor>::run() {
	constexpr float T0C = 273.15;         // [Kelvin]
	constexpr float kb  = 1.38064852e-23; // [J/K]
	constexpr float e   = 1.6021766208e-19;
	constexpr float R   = 15; // By circuit design
	constexpr float T25C = 298.15; // [Kelvin]
	constexpr float R25C = 10;     // [kOhm]
	constexpr float beta = 3435;  // [?]
	static std::vector<const char*> sensorNames = {"Poly TEMPSENS top","Poly TEMPSENS bottom","RADSENS Ana. SLDO","TEMPSENS Ana. SLDO","RADSENS Dig. SLDO","TEMPSENS Dig. SLDO","RADSENS center","TEMPSENS center"};
	
	ChipDataMap<ChipResults> results;
	auto& chipInterface = Base::chipInterface();

	uint16_t sensorConfigData;
	
	std::string line;
	std::ifstream tempFile("Results/tempCalibration.txt");
	if (tempFile.is_open())
	{
		int i = 0;
		while ( getline(tempFile,line) )
		{
			idealityFactor[i] = std::stod(line);
			i++;
		}
		tempFile.close();
	}
	else{
		LOG(INFO) << BOLDMAGENTA << "Unable to find temperature calibration, will use default\n" << RESET;
		for(int sensor=0;sensor<8;sensor++){
		LOG(INFO) << BOLDMAGENTA << sensorNames[sensor] << " = " << BOLDYELLOW << idealityFactor[sensor] << BOLDMAGENTA << "; " << RESET;
		}
	}
	std::ifstream ADCFile("Results/ADCCalibration.txt");
	if (ADCFile.is_open())
	{
		std::getline(ADCFile, line);
		ADCintercept = std::stod(line);
		std::getline(ADCFile, line);
		ADCslope = std::stod(line);
		ADCFile.close();
	}
	else{
		LOG(INFO) << BOLDMAGENTA << "Unable to find ADC calibration, will use default\n" << RESET;
		LOG(INFO) << BOLDMAGENTA << "ADCintercept = " << BOLDYELLOW << ADCintercept << BOLDMAGENTA << "; ADCslope = " << BOLDYELLOW << ADCslope << RESET;
	}

	Base::for_each_chip([&] (auto* chip) {
		auto& valueLow = results[chip].valueLow;
		auto& valueHigh = results[chip].valueHigh;
		auto& measureDV = results[chip].measureDV;
		auto& temperature = results[chip].temperature;

		for(int sensor=0;sensor<8;sensor++){
			chipInterface.WriteReg(chip, "MonitorEnable", 1); //Choose VMUX entry
			chipInterface.WriteReg(chip, "VMonitor", sensor_VMUX[sensor]);

			if(sensor >= 2){
				valueHigh = 0;
				// Get high bias voltage
				for(int sensorDEM=0;sensorDEM<16;sensorDEM+=7){
					if(sensor < 6){
						sensorConfigData = bits::pack<1, 4, 1, 1, 4, 1>(true, sensorDEM, 0, true, sensorDEM, 0);
						chipInterface.WriteReg(chip, "MON_SENS_SLDO", sensorConfigData);
					}else{ //center sensors use different bias
						sensorConfigData = bits::pack<1, 4, 1>(true, sensorDEM, 0);
						chipInterface.WriteReg(chip, "MON_SENS_ACB", sensorConfigData);
					}
					usleep(1000000);
					chipInterface.SendGlobalPulse(chip, {"ADCStartOfConversion"}); //ADC start conversion
					valueHigh += chipInterface.ReadReg(chip, "MonitoringDataADC") * ADCslope + ADCintercept;
				}
				valueHigh = valueHigh/3;
				usleep(5000000);
				valueLow = 0;
				// Get low bias voltage
				for(int sensorDEM=0;sensorDEM<16;sensorDEM+=7){
					if(sensor < 6){
						sensorConfigData = bits::pack<1, 4, 1, 1, 4, 1>(true, sensorDEM, 1, true, sensorDEM, 1);
						chipInterface.WriteReg(chip, "MON_SENS_SLDO", sensorConfigData);
					}else{ //center sensors use different bias
						sensorConfigData = bits::pack<1, 4, 1>(true, sensorDEM, 1);
						chipInterface.WriteReg(chip, "MON_SENS_ACB", sensorConfigData);
					}
					usleep(1000000);
					chipInterface.SendGlobalPulse(chip, {"ADCStartOfConversion"}); //ADC start conversion
					valueLow += chipInterface.ReadReg(chip, "MonitoringDataADC") * ADCslope + ADCintercept;
				}
				valueLow = valueLow/3;
				
				measureDV = valueLow - valueHigh;
			}else{ //Poly sensors do not have a bias difference
				usleep(1000000);
				chipInterface.SendGlobalPulse(chip, {"ADCStartOfConversion"}); //ADC start conversion
				measureDV = chipInterface.ReadReg(chip, "MonitoringDataADC") * ADCslope + ADCintercept;
			}
			temperature[sensor] = (e/(kb*log(R)))*(measureDV)/idealityFactor[sensor]-T0C;
		}
		//Get NTC temperature
		temperature[8] = chipInterface.ReadHybridTemperature(chip);

		chipInterface.WriteReg(chip, "MonitorConfig", 2); //Choose MUX entry
		chipInterface.SendGlobalPulse(chip, {"ADCStartOfConversion"}); //ADC start conversion
		double NTCvoltage = chipInterface.ReadReg(chip, "MonitoringDataADC") * ADCslope + ADCintercept;
		chipInterface.WriteReg(chip, "VMonitor", 0b000001); //Choose MUX entry
		chipInterface.WriteReg(chip, "IMonitor", 9);
		chipInterface.SendGlobalPulse(chip, {"ADCStartOfConversion"}); //ADC start conversion
		double NTCcurrent = (chipInterface.ReadReg(chip, "MonitoringDataADC") * ADCslope + ADCintercept) / 4990;
		double resistance  = NTCvoltage/NTCcurrent / 1000;              // [kOhm]
		temperature[9] = 1. / (1. / T25C + log(resistance / R25C) / beta) - T0C; // [Celsius]
	});
	return results;
}

template <class Flavor>
void RD53TsensReadoutADC<Flavor>::draw(const ChipDataMap<ChipResults>& results) const {
	for (const auto& item : results) {
		TsensReadoutADCHistograms* histos = new TsensReadoutADCHistograms;
		histos->fillTRA(
			item.second.temperature
		);
	}
}

template <class Flavor>
const int RD53TsensReadoutADC<Flavor>::sensor_VMUX[];

template class RD53TsensReadoutADC<RD53BFlavor::ATLAS>;
template class RD53TsensReadoutADC<RD53BFlavor::CMS>;

}


