#include "RD53BGainScan.h"

#include "../Utils/xtensor/xview.hpp"
#include "../Utils/xtensor/xhistogram.hpp"
#include "../Utils/xtensor/xindex_view.hpp"
#include "../Utils/xtensor/xstrided_view.hpp"
#include "../Utils/xtensor/xio.hpp"
#include "../Utils/xtensor/xoperation.hpp"
#include "../Utils/xtensor/xmath.hpp"

#include <TFile.h>
#include <TCanvas.h>
#include <TH1F.h>
#include <TH2F.h>
#include <TGaxis.h>

namespace RD53BTools {


template <class Flavor>
void RD53BGainScan<Flavor>::init() {
    vcalBins = xt::arange(param("vcalRange"_s)[0], param("vcalRange"_s)[1], param("vcalStep"_s));
}

template <class Flavor>
ChipDataMap<typename RD53BGainScan<Flavor>::ChipResult> RD53BGainScan<Flavor>::run(Task progress) const {
    ChipDataMap<ChipResult> result;

    ChipDataMap<xt::xtensor<size_t, 2>> Sx;
    ChipDataMap<xt::xtensor<size_t, 2>> Sxx;
    ChipDataMap<xt::xtensor<size_t, 2>> Sxy;
    ChipDataMap<xt::xtensor<size_t, 2>> Sy;
    ChipDataMap<xt::xtensor<size_t, 2>> Syy;
    ChipDataMap<xt::xtensor<size_t, 2>> Count;

    auto& chipInterface = Base::chipInterface();
    
    const auto& offset = param("injectionTool"_s).param("offset"_s);
    const auto& size = param("injectionTool"_s).param("size"_s);

    Base::for_each_chip([&] (auto* chip) {
        chipInterface.WriteReg(chip, Flavor::Reg::VCAL_MED, param("vcalMed"_s));
        result[chip].slope = xt::zeros<double>({size[0], size[1]});
        result[chip].intercept = xt::zeros<double>({size[0], size[1]});
        result[chip].rSquared = xt::zeros<double>({size[0], size[1]});
        Sx[chip] = xt::zeros<size_t>({size[0], size[1]});
        Sxx[chip] = xt::zeros<size_t>({size[0], size[1]});
        Sxy[chip] = xt::zeros<size_t>({size[0], size[1]});
        Sy[chip] = xt::zeros<size_t>({size[0], size[1]});
        Syy[chip] = xt::zeros<size_t>({size[0], size[1]});
        Count[chip] = xt::zeros<size_t>({size[0], size[1]});
        result[chip].Scan = xt::zeros<size_t>({vcalBins.size(), 15ul});
    });


    param("injectionTool"_s).injectionScan(
        progress, 
        makeScan(
            ScanRange(vcalBins.size(), [&] (size_t i) {
                Base::for_each_chip([&] (auto* chip) {
                    chipInterface.WriteReg(chip, Flavor::Reg::VCAL_HIGH, param("vcalMed"_s) + vcalBins(i));
                });
            })
        ),
        [&, this] (auto i, auto&& events) {
            Base::for_each_chip([&] (auto* chip) {
                for (size_t i = 0; i < vcalBins.size(); ++i) {
                    for (const auto& event : xt::view(events[chip], i, xt::all())) {
                        for (const auto& hit : event.hits) {
                            ++result[chip].Scan(i, hit.tot);
                            if (hit.tot <= 14 && param("injectionTool"_s).rowRange().contains(hit.row) && param("injectionTool"_s).colRange().contains(hit.col)) {
                                Sx[chip](hit.row - offset[0], hit.col - offset[1]) += vcalBins[i];
                                Sxx[chip](hit.row - offset[0], hit.col - offset[1]) += vcalBins[i] * vcalBins[i];
                                Sy[chip](hit.row - offset[0], hit.col - offset[1]) += hit.tot;
                                Syy[chip](hit.row - offset[0], hit.col - offset[1]) += hit.tot * hit.tot;
                                Sxy[chip](hit.row - offset[0], hit.col - offset[1]) += vcalBins[i] * hit.tot;
                                ++Count[chip](hit.row - offset[0], hit.col - offset[1]);
                            }
                        }
                    } 
                }
            });
        }
    );

    Base::for_each_chip([&] (auto* chip) {
        result[chip].slope = 
            xt::cast<double>(Count[chip] * Sxy[chip] - Sx[chip] * Sy[chip]) / 
            (Count[chip] * Sxx[chip] - xt::square(Sx[chip]));

        result[chip].intercept =  
            Sy[chip] / xt::cast<double>(Count[chip]) - 
            Sx[chip] * result[chip].slope / xt::cast<double>(Count[chip]);
        
        result[chip].rSquared = xt::square(
            (Count[chip] * Sxy[chip] - Sx[chip] * Sy[chip]) / 
            xt::sqrt((Count[chip] * Sxx[chip] - xt::square(Sx[chip])) * (Count[chip] * Syy[chip] - xt::square(Sy[chip])))
        );
    });

    return result;
}

template <class Flavor>
void RD53BGainScan<Flavor>::draw(const ChipDataMap<ChipResult>& result) {
    Base::createRootFile();
    
    auto usedPixels = param("injectionTool"_s).usedPixels();

    Base::for_each_chip([&] (RD53B<Flavor>* chip) {
        Base::createRootFileDirectory(chip);

        Base::drawHist2D(result.at(chip).Scan, "ToT vs VCal", param("vcalRange"_s)[0], param("vcalRange"_s)[1], 0, 15, "VCal", "ToT", "Count");

        auto enabled = xt::view(usedPixels && chip->injectablePixels(), param("injectionTool"_s).rowRange(), param("injectionTool"_s).colRange());

        Base::drawMap(xt::where(enabled, result.at(chip).slope, 0), "ToT Gain Map", "Slope", param("injectionTool"_s).offset(0), param("injectionTool"_s).offset(1));
        xt::xtensor<double, 1> filtered_slope = xt::filter(result.at(chip).slope, enabled);
        Base::drawHist(filtered_slope, "ToT Gain Distribution", 50, 0, 1.1 * xt::amax(filtered_slope)(), "Slope", false);

        std::cout << result.at(chip).slope << std::endl;
        std::cout << result.at(chip).intercept << std::endl;

        Base::drawMap(xt::where(enabled, result.at(chip).intercept, 0), "ToT Offset Map", "Intercept", param("injectionTool"_s).offset(0), param("injectionTool"_s).offset(1));
        xt::xtensor<double, 1> filtered_intercept = xt::filter(result.at(chip).intercept, enabled);
        auto max_intercept = xt::amax(filtered_intercept)();
        auto min_intercept = xt::amax(filtered_intercept)();
        auto intercept_width = max_intercept - min_intercept;

        Base::drawHist(
            filtered_intercept, 
            "ToT Offset Distribution",
            50, 
            min_intercept - 0.1 * intercept_width, 
            max_intercept + 0.1 * intercept_width, 
            "Intercept",
            false
        );


        Base::drawMap(xt::where(enabled, result.at(chip).rSquared, 0), "r-squared Map", "r-squared", param("injectionTool"_s).offset(0), param("injectionTool"_s).offset(1));
        xt::xtensor<double, 1> filtered_rSquared = xt::filter(result.at(chip).rSquared, enabled);
        Base::drawHist(filtered_rSquared, "r-squared Distribution", 50, 0, 1, "r-squared", false);

    });
}

template class RD53BGainScan<RD53BFlavor::ATLAS>;
template class RD53BGainScan<RD53BFlavor::CMS>;

}