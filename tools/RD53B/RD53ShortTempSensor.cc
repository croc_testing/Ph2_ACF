#include "RD53ShortTempSensor.h"

#include "../ProductionTools/ITchipTestingInterface.h"

#include "../DQMUtils/RD53ShortTempSensorHistograms.h"


namespace RD53BTools {


template <class Flavor>
ChipDataMap<typename RD53ShortTempSensor<Flavor>::ChipResults> RD53ShortTempSensor<Flavor>::run() {
	constexpr float T0C = 273.15;         // [Kelvin]
	constexpr float kb  = 1.38064852e-23; // [J/K]
	constexpr float e   = 1.6021766208e-19;
	constexpr float R   = 15; // By circuit design
	constexpr float T25C = 298.15; // [Kelvin]
	constexpr float R25C = 10;     // [kOhm]
	constexpr float beta = 3435;  // [?]
	
	ChipDataMap<ChipResults> results;
	auto& chipInterface = Base::chipInterface();

	Ph2_ITchipTesting::ITpowerSupplyChannelInterface dKeithley2410(Base::system().fPowerSupplyClient, "TestKeithley", "Front");
	dKeithley2410.setupKeithley2410ChannelSense(VOLTAGESENSE, 2.0);

	uint16_t sensorConfigData;
	std::string line;
	std::ifstream myfile("Results/tempCalibration.txt");
	if (myfile.is_open())
	{
		int i = 0;
		while ( getline(myfile,line) )
		{
			idealityFactor[i] = std::stod(line);
			i++;
		}
		myfile.close();
	}
	else{
		std::cout << "Unable to find calibration, will use default\n";
	}

	Base::for_each_chip([&] (auto* chip) {
		auto& valueLow = results[chip].valueLow;
		auto& valueHigh = results[chip].valueHigh;
		auto& measureDV = results[chip].measureDV;
		auto& temperature = results[chip].temperature;

		for(int sensor=0;sensor<8;sensor++){
			chipInterface.WriteReg(chip, "MonitorEnable", 1); //Choose VMUX entry
			chipInterface.WriteReg(chip, "VMonitor", sensor_VMUX[sensor]);

			if(sensor >= 2){
				valueHigh = 0;
				// Get high bias voltage
				for(int sensorDEM=0;sensorDEM<16;sensorDEM+=7){
					if(sensor < 6){
						sensorConfigData = bits::pack<1, 4, 1, 1, 4, 1>(true, sensorDEM, 0, true, sensorDEM, 0);
						chipInterface.WriteReg(chip, "MON_SENS_SLDO", sensorConfigData);
					}else{ //center sensors use different bias
						sensorConfigData = bits::pack<1, 4, 1>(true, sensorDEM, 0);
						chipInterface.WriteReg(chip, "MON_SENS_ACB", sensorConfigData);
					}
					valueHigh += dKeithley2410.getVoltage();
				}
				valueHigh = valueHigh/3;
				
				valueLow = 0;
				// Get low bias voltage
				for(int sensorDEM=0;sensorDEM<16;sensorDEM+=7){
					if(sensor < 6){
						sensorConfigData = bits::pack<1, 4, 1, 1, 4, 1>(true, sensorDEM, 1, true, sensorDEM, 1);
						chipInterface.WriteReg(chip, "MON_SENS_SLDO", sensorConfigData);
					}else{ //center sensors use different bias
						sensorConfigData = bits::pack<1, 4, 1>(true, sensorDEM, 1);
						chipInterface.WriteReg(chip, "MON_SENS_ACB", sensorConfigData);
					}
					valueLow += dKeithley2410.getVoltage();
				}
				valueLow = valueLow/3;
				measureDV = valueLow - valueHigh;
			}else{ //Poly sensors do not have a bias difference
				measureDV = dKeithley2410.getVoltage();
			}
			
			temperature[sensor] = (e/(kb*log(R)))*(measureDV)/idealityFactor[sensor]-T0C;
		}
		//Get NTC temperature
		temperature[8] = chipInterface.ReadHybridTemperature(chip);

		chipInterface.WriteReg(chip, "MonitorConfig", 2); //Choose MUX entry
		double NTCvoltage = dKeithley2410.getVoltage();
		chipInterface.WriteReg(chip, "VMonitor", 0b000001); //Choose MUX entry
		chipInterface.WriteReg(chip, "IMonitor", 9);
		double NTCcurrent = dKeithley2410.getVoltage() / 4990;
		double resistance  = NTCvoltage/NTCcurrent / 1000;              // [kOhm]
		temperature[9] = 1. / (1. / T25C + log(resistance / R25C) / beta) - T0C; // [Celsius]
	});
	return results;
}

template <class Flavor>
void RD53ShortTempSensor<Flavor>::draw(const ChipDataMap<ChipResults>& results) const {
	for (const auto& item : results) {
		ShortTempSensorHistograms* histos = new ShortTempSensorHistograms;
		histos->fillSTS(
			item.second.temperature
		);
	}
}

template <class Flavor>
const int RD53ShortTempSensor<Flavor>::sensor_VMUX[];

template class RD53ShortTempSensor<RD53BFlavor::ATLAS>;
template class RD53ShortTempSensor<RD53BFlavor::CMS>;

}


