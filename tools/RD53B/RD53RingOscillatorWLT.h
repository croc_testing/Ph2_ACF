#ifndef RD53RingOscillatorWLT_H
#define RD53RingOscillatorWLT_H

#include "RD53BTool.h"


namespace RD53BTools {
template <class>
struct RD53RingOscillatorWLT;

template <class Flavor>
const auto ToolParameters<RD53RingOscillatorWLT<Flavor>> = make_named_tuple(
    std::make_pair("vmeter"_s, std::string("TestKeithley")),
    std::make_pair("vmeterCH"_s, std::string("Front")),
    std::make_pair("gateWidth"_s, uint16_t{100}),
    std::make_pair("minVDDD"_s, 0),
    std::make_pair("maxVDDD"_s, 15),
    std::make_pair("abortVDDD"_s, INFINITY)
);

template <class Flavor>
struct RD53RingOscillatorWLT : public RD53BTool<RD53RingOscillatorWLT, Flavor> {
    using Base = RD53BTool<RD53RingOscillatorWLT, Flavor>;
    using Base::Base;
    using Base::param;


public:
    struct ChipResults {
        double trimOscCounts[42][16];
        double trimOscFrequency[42][16];
        double trimVoltage[16];
        int n;
        int countEnableTimeBX;

        ChipResults() : n{0} {}
    };

private:
    uint16_t pulseRoute, pulseWidth, nBX;

public:
    void init();

    ChipDataMap<ChipResults> run() const;

    void draw(const ChipDataMap<ChipResults>& results);

};

}

#endif


