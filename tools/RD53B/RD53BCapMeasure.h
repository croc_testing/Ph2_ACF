#ifndef RD53BCapMeasure_H
#define RD53BCapMeasure_H

#include "RD53BTool.h"

//#include "../ProductionTools/ITchipTestingInterface.h"

namespace RD53BTools
{
template <class>
struct RD53BCapMeasure; // forward declaration

template <class Flavor>
const auto ToolParameters<RD53BCapMeasure<Flavor>> = make_named_tuple(std::make_pair("powerSupplyName"_s, std::string("TestKeithley")),
                                                                      std::make_pair("resistance"_s, 4990.0));

template <class Flavor>
struct RD53BCapMeasure : public RD53BTool<RD53BCapMeasure, Flavor> {
    using Base = RD53BTool<RD53BCapMeasure, Flavor>;
    using Base::Base;
    using Base::param;

    struct CapVoltages {
        double CapVolts[4];
    };

    using capVoltages = ChipDataMap<RD53BCapMeasure::CapVoltages>;
    capVoltages run() const;

    void draw(const capVoltages& results) const;

};

}

#endif


