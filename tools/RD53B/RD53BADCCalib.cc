#include "RD53BADCCalib.h"

#include "TAxis.h"
#include "TDirectory.h"
#include "TF1.h"
#include "TFile.h"
#include "TGraph.h"
#include "TGraphErrors.h"
#include "TTree.h"

#include <array>
#include <cmath>
#include <fstream>
#include <sstream>
#include <stdexcept>
#include <string>
#include <utility>
#include <vector>

namespace RD53BTools {

template <class F>
void RD53BADCCalib<F>::init() {
    psId = param("powerSupplyId"_s);
    psChId = param("powerSupplyChannelId"_s);
    min = param("minVoltage"_s);
    max = param("maxVoltage"_s);
    if(min > max) throw std::logic_error("Bad configuration: minimum voltage > maximum voltage.");
    if(min < 0.0f) throw std::logic_error("Bad configuration: minimum voltage is negative.");
    if(max > 1.3f) throw std::logic_error("Bad configuration: maximum voltage is dangerously high.");
    nVolt = param("nVoltages"_s);
    nSamp = param("adcSamples"_s);
    psMsg = "GetVoltage,PowerSupplyId:" + psId + ",ChannelId:" + psChId;
    if(param("powerSupplySamples"_s) > 1) psMsg += ",Stats,Samples:" + std::to_string(param("powerSupplySamples"_s));
}

template <class F>
typename RD53BADCCalib<F>::Results RD53BADCCalib<F>::run() {
    Results res;

    /* additional setup in case a Keithley SMU is being used */
    Base::system().fPowerSupplyClient->sendAndReceivePacket(
        "K2410:SetSpeed"
        ",PowerSupplyId:" + psId +
        ",ChannelId:" + psChId +
        ",IntegrationTime:0.01"
    );

    Base::for_each_chip([this, &res](Chip* chip) { this->calibChip(chip, res[chip]); });

    return res;
}

template <class F>
void RD53BADCCalib<F>::draw(const Results& results) {
    std::ofstream json(Base::getOutputFilePath("results.json"));
    // TFile f(Base::getAvailablePath(".root").c_str(), "RECREATE");
    Base::createRootFile();

    uint16_t boardId, hybridId, chipLane;
    float setV, measV;
    float errMeasV = NAN;

    std::vector<short> adcRdng;
    adcRdng.reserve(nSamp);

    TTree* t = new TTree("calibData", "Calibration data");
    t->Branch("boardId", &boardId);
    t->Branch("hybridId", &hybridId);
    t->Branch("chipLane", &chipLane);
    t->Branch("setV", &setV);
    t->Branch("measV", &measV);
    t->Branch("errMeasV", &errMeasV);
    t->Branch("adcRdng", &adcRdng);

    TF1 line("line", "[V(0)]+[dV/dADC]*x");
    std::vector<float> x, xErr, spread;

    const char* str = "{";
    json << std::scientific << "{\"chips\":[";
    for(const std::pair<const ChipLocation, Data>& chipRes: results) {
        const std::vector<float>& y = chipRes.second.psData[0];
        const std::vector<float>& yErr = chipRes.second.psData[1];

        if(y.empty()) continue;

        json << str;
        str = ",{";

        boardId = chipRes.first.board_id;
        hybridId = chipRes.first.hybrid_id;
        chipLane = chipRes.first.chip_lane;

        json << "\"board\":" << boardId << ","
                << "\"hybrid\":" << hybridId << ","
                << "\"lane\":" << chipLane << ",";

        Base::createRootFileDirectory(chipRes.first);
        // f.mkdir(("board_" + std::to_string(boardId)).c_str(), "", true)
        //     ->mkdir(("hybrid_" + std::to_string(hybridId)).c_str(), "", true)
        //     ->mkdir(("chip_" + std::to_string(chipId)).c_str(), "", true)
        //     ->cd();

        x.reserve(y.size());
        xErr.reserve(y.size());
        spread.reserve(y.size());
        x.clear();
        xErr.clear();
        spread.clear();
        for(const std::pair<const float, std::map<short, unsigned int>>& p: chipRes.second.adcData) {
            short min = 4096;
            short max = -1;
            unsigned long long n = 0;
            unsigned long long sum = 0;
            unsigned long long sum2 = 0;
            adcRdng.clear();
            for(const std::pair<const short, unsigned int>& pp: p.second) {
                if(pp.first < min) min = pp.first;
                if(pp.first > max) max = pp.first;
                unsigned long long buf = pp.first * pp.second;
                sum += buf;
                sum2 += buf * pp.first;
                n += pp.second;
                adcRdng.insert(adcRdng.end(), pp.second, pp.first);
            }
            x.push_back(sum / static_cast<double>(n));
            if(n > 1) {
                xErr.push_back(std::sqrt((sum2 * n - sum * sum) / static_cast<double>(n - 1)) / n);
            }
            spread.push_back(max - min + 1);
            setV = p.first;
            measV = y[x.size() - 1];
            if(!yErr.empty()) errMeasV = yErr[x.size() - 1];
            t->Fill();
        }

        TGraph gSpread(y.size(), y.data(), spread.data());
        gSpread.SetName("adcSpread");
        gSpread.SetTitle("A/D conversions spread");
        gSpread.GetXaxis()->SetTitle("Injected voltage [V]");
        gSpread.GetYaxis()->SetTitle("Spread [ADC counts]");
        gSpread.Write();

        float m = (y.back() - y.front()) / (x.back() - x.front());
        float q = y.front() - m * x.front();
        line.SetParameter("V(0)", q);
        line.SetParameter("dV/dADC", m);

        TGraph* g;
        if(xErr.empty() && yErr.empty()) {
            g = new TGraph(x.size(), x.data(), y.data());
        }
        else if(xErr.empty()) {
            g = new TGraphErrors(x.size(), x.data(), y.data(), nullptr, yErr.data());
        }
        else if(yErr.empty()) {
            g = new TGraphErrors(x.size(), x.data(), y.data(), xErr.data(), nullptr);
        }
        else {
            g = new TGraphErrors(x.size(), x.data(), y.data(), xErr.data(), yErr.data());
        }
        g->SetName("adcCalib");
        g->SetTitle("ADC calibration");
        g->GetXaxis()->SetTitle("ADC conversion [ADC counts]");
        g->GetYaxis()->SetTitle("Injected voltage [V]");
        g->Fit(&line, "Q");
        g->Write();

        q = line.GetParameter("V(0)");
        m = line.GetParameter("dV/dADC");

        for(int i = 0; i < g->GetN(); ++i)
        {
            g->SetPointY(i, y[i] - q - m * x[i]);
        }

        g->SetName("adcResid");
        g->SetTitle("Residuals");
        g->GetXaxis()->SetTitle("ADC conversion [ADC counts]");
        g->GetYaxis()->SetTitle("Residual (voltage) [V]");
        g->Write();

        json << "\"fitted_line\":{\"intercept\":" << q << ",\"slope\":" << m << "},"
                << "\"volt\":[";
        const char* str2 = "";
        for(const float& y : y) {
            json << str2 << y;
            str2 = ",";
        }
        json << "],\"conv\":[";
        str2 = "";
        for(const float& x : x) {
            json << str2 << x;
            str2 = ",";
        }
        json << "]";
        if(!xErr.empty()) {
            json << ",\"conv_err\":[";
            str2 = "";
            for(const float& xErr : xErr) {
                json << str2 << xErr;
                str2 = ",";
            }
            json << "]";
        }
        if(!yErr.empty()) {
            json << ",\"volt_err\":[";
            str2 = "";
            for(const float& yErr : yErr) {
                json << str2 << yErr;
                str2 = ",";
            }
            json << "]";
        }
        json << "}"; // end of chip data
    }
    json << "]}";

    t->Write();
}


template <class F>
void RD53BADCCalib<F>::calibChip(Chip* chip, Data& res) {
    auto& chipIface = *static_cast<RD53BInterface<F>*>(Base::system().fReadoutChipInterface);
    TCPClient& psIface = *Base::system().fPowerSupplyClient;

    double groundOffset = 0.0;

    if(param("groundCorrection"_s)) {
        psIface.sendAndReceivePacket("TurnOff,PowerSupplyId:" + psId + ",ChannelId:" + psChId);

        chipIface.WriteReg(chip, F::Reg::MonitorConfig, (uint8_t)RD53B<F>::IMux::HIGH_Z << 6 | (uint8_t)RD53B<F>::VMux::GNDA);
        psIface.sendAndReceivePacket(
            "VoltmeterSetRange"
            ",VoltmeterId:" + param("voltmeterId"_s) +
            ",ChannelId:" + param("voltmeterChannelId"_s) +
            ",Value:1.3"
        );
        groundOffset = std::stod(psIface.sendAndReceivePacket(
            "ReadVoltmeter"
            ",VoltmeterId:" + param("voltmeterId"_s) +
            ",ChannelId:" + param("voltmeterChannelId"_s)
        ));
        LOG(INFO) << BOLDBLUE << "Analog ground offset = " << BOLDYELLOW << groundOffset << " V" << RESET;

        chipIface.WriteReg(chip, F::Reg::MonitorConfig, (uint8_t)RD53B<F>::IMux::HIGH_Z << 6 | (uint8_t)RD53B<F>::VMux::VREF_ADC);
        std::string adcRef(psIface.sendAndReceivePacket(
            "ReadVoltmeter"
            ",VoltmeterId:" + param("voltmeterId"_s) +
            ",ChannelId:" + param("voltmeterChannelId"_s)
        ));
        LOG(INFO) << BOLDBLUE << "ADC reference voltage = " << BOLDYELLOW << adcRef << " V" << RESET;
    }

    chipIface.WriteReg(chip, F::Reg::MonitorConfig, 1u << 12 | (uint8_t)RD53B<F>::IMux::HIGH_Z << 6 | (uint8_t)RD53B<F>::VMux::HIGH_Z);

    /* additional setup in case a Keithley SMU is being used */
    psIface.sendAndReceivePacket(
        "K2410:setupVsource"
        ",PowerSupplyId:" + psId +
        ",ChannelId:" + psChId +
        ",Voltage:" + std::to_string(min) +
        ",CurrCompl:1e-6"
    );

    for(int i = 0; i < nVolt; ++i) {
        float volt = (max * i + min * (nVolt - 1 - i)) / static_cast<double>(nVolt - 1);
        psIface.sendAndReceivePacket("TurnOff,PowerSupplyId:" + psId + ",ChannelId:" + psChId);
        psIface.sendAndReceivePacket("SetVoltage,PowerSupplyId:" + psId + ",ChannelId:" + psChId + ",Voltage:" + std::to_string(volt));
        psIface.sendAndReceivePacket("TurnOn,PowerSupplyId:" + psId + ",ChannelId:" + psChId);

        double arr[2];
        std::istringstream resp(psIface.sendAndReceivePacket(psMsg));
        resp >> arr[0];
        if(param("powerSupplySamples"_s) > 1) resp >> arr[1];

        if(!resp) continue;

        std::map<short, unsigned int> m;
        for(int j = 0; j < nSamp; ++j) {
            unsigned short x = chipIface.ReadChipADC(static_cast<RD53B<F>*>(chip), "HIGH_Z");
            if(param("abortOnLimit"_s) && (x == 0 || x == 4095)) {
                m.clear();
                break;
            }
            m[x] += 1;
        }
        if(!m.empty()) {
            res.adcData.emplace(volt, std::move(m));
            res.psData[0].push_back(arr[0] - groundOffset);
            if(param("powerSupplySamples"_s) > 1) res.psData[1].push_back(arr[1] / std::sqrt(param("powerSupplySamples"_s)));
        }
    }

    psIface.sendAndReceivePacket("TurnOff,PowerSupplyId:" + psId + ",ChannelId:" + psChId);
}

template class RD53BADCCalib<RD53BFlavor::ATLAS>;
template class RD53BADCCalib<RD53BFlavor::CMS>;

}

