#ifndef RD53TempSensor_H
#define RD53TempSensor_H

#include "RD53BTool.h"

namespace RD53BTools {

template <class Flavor>
struct RD53TempSensor : public RD53BTool<RD53TempSensor, Flavor> {
    using Base = RD53BTool<RD53TempSensor, Flavor>;
    using Base::Base;
	
    struct ChipResults {
		double idealityFactor[8];
		double valueLow;
		double valueHigh;
		double calibDV[8][2];
		double calibNTCtemp[8][2];
		double calibSenstemp[8][2];
		double NTCvoltage;
    };

	
	static constexpr double power[2] = {1.06, 1.68};
	static constexpr int sensor_VMUX[8] = {0b1000000000101, 0b1000000000110, 0b1000000001101, 0b1000000001110, 0b1000000001111, 0b1000000010000, 0b1000000010001, 0b1000000010010};
	

    ChipDataMap<ChipResults> run();

    void draw(const ChipDataMap<ChipResults>& results) const;
};

}

#endif


