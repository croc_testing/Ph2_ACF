/*!
  \file                  RD53TsensCalibrationHistograms.h
  \brief                 Header file of TsensCalibration histograms
  \author                Umberto MOLINATTI
  \version               1.0
  \date                  12/07/21
  Support:               email to umberto.molinatti@cern.ch
*/

#ifndef RD53TsensCalibrationHistograms_H
#define RD53TsensCalibrationHistograms_H

#include "../Utils/ContainerFactory.h"
#include "../Utils/ContainerStream.h"
#include "../Utils/GenericDataArray.h"
#include "DQMHistogramBase.h"

#include "TFitResult.h"
#include "TGraph.h"
#include <TStyle.h>
#include <fstream>

#include "TF1.h"
#include "TH1.h"
#include "TMultiGraph.h"
#include <stdio.h>
#include <time.h>

#define LOGNAME_FORMAT "%d%m%y_%H%M%S"
#define LOGNAME_SIZE 50

class TsensCalibrationHistograms
{
  public:
    void fillTC( const double (&idealityFactor)[8], const double (&calibNTCtemp)[8][2], const double (&calibSenstemp)[8][2], const double (&power)[2] );
	
  private:
    DetectorDataContainer DetectorData;
};

#endif
