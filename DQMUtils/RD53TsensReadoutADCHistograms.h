/*!
  \file                  RD53TsensReadoutADCHistograms.h
  \brief                 Header file of TsensReadoutADC histograms
  \author                Umberto MOLINATTI
  \version               1.0
  \date                  04/07/22
  Support:               email to umberto.molinatti@cern.ch
*/

#ifndef RD53TsensReadoutADCHistograms_H
#define RD53TsensReadoutADCHistograms_H

#include "../Utils/ContainerFactory.h"
#include "../Utils/ContainerStream.h"
#include "../Utils/GenericDataArray.h"
#include "DQMHistogramBase.h"

#include "TGraph.h"
#include "TFitResult.h"
#include <TStyle.h>
#include <fstream>
#include "TObject.h"

class TsensReadoutADCHistograms
{
  public:
    void fillTRA(const double (&temperature)[10]);
	
  private:
    DetectorDataContainer DetectorData;
};

#endif
