#ifndef OSTREAM_INDENT_H
#define OSTREAM_INDENT_H

#include <ostream>

namespace ostream_indent {

/* Helper function to get a storage index in a stream */
inline int get_indent_index() {
    /* ios_base::xalloc allocates indices for custom-storage locations. These indices are valid for all streams */
    static int index = std::ios_base::xalloc();
    return index;
}

inline std::ios_base& increase_indent(std::ios_base& stream) {
    /* The iword(index) function gives a reference to the index-th custom storage location as a integer */
    stream.iword(get_indent_index())++;
    return stream;
}

inline std::ios_base& decrease_indent(std::ios_base& stream) {
    /* The iword(index) function gives a reference to the index-th custom storage location as a integer */
    stream.iword(get_indent_index())--;
    return stream;
}

// template<class charT, class traits>
inline std::ostream& endl_indent(std::ostream& stream) {
    int indent = stream.iword(get_indent_index());
    stream.put(stream.widen('\n'));
    while (indent) {
        for (int i = 0; i < 4; ++i)
            stream.put(stream.widen(' '));
        indent--;
    }
    stream.flush();
    return stream;
}

} // namespace ostream_indent

#endif